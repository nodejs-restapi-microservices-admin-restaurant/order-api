# Documentation
* [Order API](#order-api)

## Order API
### GET /orders
Returns all Order instance.

Example response:
```json
[
    {
        "orderPrice": 100,
        "onKitchen": false,
        "completed": false,
        "_id": "5dd15d91f3c8a07baa5ee90f",
        "staff": {
            "isAdmin": false,
            "_id": "5dd071bc30a9086cae1b76b5",
            "email": "user@test.com",
            "password": "$2b$10$pq0QweG6QwTDus6VzAh3YekwJLWE6zOdPkfGKeXCDULAupWRCWMky",
            "__v": 0
        },
        "table": null,
        "orderItems": [
            {
                "ingredients": [
                    {
                        "price": null,
                        "_id": "5dd09c790cdb777077a0becc",
                        "title": "Сахар",
                        "restInStock": null,
                        "description": "",
                        "__v": 0
                    },
                    {
                        "price": null,
                        "_id": "5dd0a21d40d38b71a3adc7e2",
                        "title": "Белый хлеб",
                        "restInStock": null,
                        "description": "",
                        "__v": 0
                    }
                ],
                "price": 100,
                "_id": "5dd15d91f3c8a07baa5ee910",
                "title": "aaaa"
            }
        ],
        "created_at": "2019-11-17T14:47:45.316Z",
        "updated_at": "2019-11-17T14:47:45.316Z",
        "__v": 0
    }
]
```

### POST /orders
Adds to Order instance in a database.

|Param|Type|
|--|--|
|staff|`string`|
|table|`number`|
|orderItems|`array`|
|orderPrice|`number`|
|onKitchen|`boolean`|
|completed|`boolean`|

Example request:
```json
{
    "orderPrice": 100,
    "onKitchen": true,
    "completed": false,
    "staff": "5dd071bc30a9086cae1b76b5",
    "table": 12,
    "orderItems": ["5dd09c790cdb777077a0becc"]
}
```
Example response:
```json
{
    "message": "Successfully created!"
}
```

### GET /orders/:id
Returns a Order instance by id.

Example response:
```json
{
    "orderPrice": 100,
    "onKitchen": false,
    "completed": false,
    "_id": "5dd15d91f3c8a07baa5ee90f",
    "staff": {
        "isAdmin": false,
        "_id": "5dd071bc30a9086cae1b76b5",
        "email": "user@test.com",
        "password": "$2b$10$pq0QweG6QwTDus6VzAh3YekwJLWE6zOdPkfGKeXCDULAupWRCWMky",
        "__v": 0
    },
    "table": null,
    "orderItems": [
        {
            "ingredients": [
                {
                    "price": null,
                    "_id": "5dd09c790cdb777077a0becc",
                    "title": "Сахар",
                    "restInStock": null,
                    "description": "",
                    "__v": 0
                },
                {
                    "price": null,
                    "_id": "5dd0a21d40d38b71a3adc7e2",
                    "title": "Белый хлеб",
                    "restInStock": null,
                    "description": "",
                    "__v": 0
                }
            ],
            "price": 100,
            "_id": "5dd15d91f3c8a07baa5ee910",
            "title": "aaaa"
        }
    ],
    "created_at": "2019-11-17T14:47:45.316Z",
    "updated_at": "2019-11-17T14:47:45.316Z",
    "__v": 0
}
```
### PUT /orders/:id
Updates to Order instance in a database.

|Param|Type|
|--|--|
|staff|`string`|
|table|`number`|
|orderItems|`array`|
|orderPrice|`number`|
|onKitchen|`boolean`|
|completed|`boolean`|

Example request:
```json
{
    "orderPrice": 100,
    "onKitchen": true,
    "completed": false,
    "staff": "5dd071bc30a9086cae1b76b5",
    "table": 12,
    "orderItems": ["5dd09c790cdb777077a0becc"]
}
```
Example response:
```json
{
    "message": "Successfully updated!"
}
```
### DELETE /orders/:id
Delete a Order instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
